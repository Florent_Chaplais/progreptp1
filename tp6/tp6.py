# -*- coding: utf-8 -*-
"""
Created on Tue Mar 17 13:17:54 2020

prog repartie tp6: chunk

@author: florent
"""

from random import randrange
import json
import os
import socket
import itertools

MEMBER_LIST = []
HOST = 'localhost'
PORT = 8000

class MyChunk():
    """
        class chunk
    """
    def __init__(self, data, number, filename):
        self.data = data
        self.number = number
        self.filename = filename + '_' + number + '.tmp'
        self.len = len(data)
        m_dict = {'filename' : filename, 'value' : {'complete': bool, 'chunk_number' : [data]}}
        save_dico_as_json(m_dict)

def save_dico_as_json(dictionnaire):
    '''

        save_dico_as_json save dictionary in json file

    '''
    with open("chunks/" + dictionnaire["filename"], "w") as file:
        json.dump(dictionnaire, file)

def parse_files():
    """
        function parse
    """
    print('parse')
    dir_name = 'chunks'
    for file in os.listdir(dir_name):
        full_filename = "%s/%s" % (dir_name, file)
        with open(full_filename, 'r') as json_file:
            m_dict = json.load(json_file)
            MEMBER_LIST.append(m_dict)



def update_files(sources, file_dict):
    """
        function update  
        
        # comprend pas le file_dict
    """
    print('uptade')
    MEMBER_LIST.append(sources)
    m_sorted = list(MEMBER_LIST for member, _ in itertools.groupby(member))
    MEMBER_LIST.clear()
    MEMBER_LIST.append(m_sorted)


    for member in MEMBER_LIST:
        save_dico_as_json(member)
        broadcast()

    print(MEMBER_LIST)


def download_strategy():
    """
        function download
        
        # pas fini
    """
    print('download')
 
def broadcast():
    '''

        broadcast envoye au autre la liste
        
        # pas fini

    '''
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((HOST, PORT))
    #sock.send(json.dumps(MEMBER_LIST))
    print(json.dumps(MEMBER_LIST), "\nfile_dict")

def server():
    '''

        server se met en mode serveur
        
        # pas fini

    '''
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((HOST, PORT))
    sock.listen(1)

    conn, addr = sock.accept()
    print('Connected by', addr)
    while 1:
        data = conn.recv(4096)
        file_dict = conn.recv(4096)
        if not data:
            break
        print(data, file_dict)
        update_files(data, file_dict)
    conn.close()

MY_CHUNK = MyChunk('vhjrseduidohfgqeuiofhn', randrange(50), 'test')
print(MY_CHUNK.len)

#server()
