'''

    TP 5

'''

import json
import pickle
import os
import socket
import itertools

MEMBER_LIST = []
HOST = 'localhost'
PORT = 8000

def forge_member(m_id, m_name, m_ip, m_port, key_filename):
    '''

        forge_member that create dictionary from data to json

    '''
    m_dict = {"id": m_id, "name": m_name, "ip": m_ip, "port": m_port, "key_filename": key_filename}
    save_dico_as_json(m_dict)

    broadcast()

def save_dico_as_json(dictionnaire):
    '''

        save_dico_as_json save dictionary in json file

    '''
    with open("trusted_nodes/" + dictionnaire["id"] + ".json", "w") as file:
        json.dump(dictionnaire, file)

def parse_confs(dir_name):
    '''

        parse_confs parse json to disctionary and add it to MEMBER_LIST

    '''
    for file in os.listdir(dir_name):
        full_filename = "%s/%s" % (dir_name, file)
        with open(full_filename, 'r') as json_file:
            m_dict = json.load(json_file)
            MEMBER_LIST.append(m_dict)


def update_members(member_list):
    '''

        update_members update MEMBER_LIST and broadcast everybody

    '''
    MEMBER_LIST.append(member_list)
    m_sorted = list(MEMBER_LIST for member, _ in itertools.groupby(member))
    MEMBER_LIST.clear()
    MEMBER_LIST.append(m_sorted)


    for member in MEMBER_LIST:
        save_dico_as_json(member)

    print(MEMBER_LIST)

def broadcast():
    '''

        broadcast envoye au autre la liste

    '''
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((HOST, PORT))
    sock.send(json.dumps(MEMBER_LIST))

def server():
    '''

        server se met en mode serveur

    '''
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((HOST, PORT))
    sock.listen(1)

    conn, addr = sock.accept()
    print('Connected by', addr)
    while 1:
        data = conn.recv(4096)
        if not data:
            break
        print(data)
        #update_members(data)
    conn.close()


server()
