import urllib.request as ulib
import concurrent.futures as con
import cv2 as openCv

PIECES=16

def load_url(url, name):
    ulib.urlretrieve(url, 'img/' + str(name) + '.jpg')

def fast_dl(url, np_thread):
    with con.ThreadPoolExecutor(max_workers=np_thread) as e2:
        for i in range(0, 10, 1):
            e2.submit(load_url, url, i)

def cut_image(image, pieces):
    img = openCv.imread(image, 0)

def fast_Laplacian(target_image, nb_thread):
    with con.ProcessPoolExecutor(max_workers=np_thread) as e1:
        e1.submit(cut_image, image, PIECES)


fast_dl('https://picsum.photos/300/400', 5)
