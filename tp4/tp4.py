import sys
import tp4_functions as func
import xmlrpc.client

def outside_even(n):
    with xmlrpc.client.ServerProxy("http://localhost:8000/") as proxy:
        return proxy.is_prime(n)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Veuillez entrer un entier')
    else:
        num_list = func.generate_number_list(sys.argv[1])
        print(outside_even(num_list))