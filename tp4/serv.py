from xmlrpc.server import SimpleXMLRPCServer
import tp4_functions as func

server = SimpleXMLRPCServer(("localhost", 8000))
print("Listening on port 8000...")
server.register_function(func.is_prime, "is_prime")
server.serve_forever()