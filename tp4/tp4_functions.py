"""
    Functions of the TP
"""
import numpy as np

def generate_number_list(number):
    """
        Main function of the tp
    """
    try:
        number = int(number)
        number_list = np.random.rand(number)
        number_list *= 100.0
        number_list = number_list.astype(int)
        number_list = list(number_list)

        for i in range(0, len(number_list))
            number_list[i] = int(number_list[i])

        return number_list
    except TypeError as exception:
        print('Veuillez entre un entier valide', exception)


def is_prime(number_list):
    """
        Test is the list got prime number
    """
    prime = list()
    for i in number_list:
        if is_prime_number(i):
            prime.append(i)

    return prime


def is_prime_number(number):
    """
        Test is a number is prime
    """
    if number <= 1:
        return False
    if number <= 3:
        return True

    if number % 2 == 0 or number % 3 == 0:
        return False

    i = 5
    while i * i <= number:
        if (number % i == 0 or number % (i + 2) == 0):
            return False
        i = i + 6

    return True
