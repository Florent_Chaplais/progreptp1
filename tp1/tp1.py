"""
    TP1 du module programmation répartie
"""
def dicho_sort(input_list):
    if len(input_list) <= 1:
        return input_list

    list_size = len(input_list) // 2
    start_input_list = input_list[:list_size]
    end_input_list = input_list[list_size:]

    sort_start_input_list = list(dicho_sort(start_input_list))
    sort_end_input_list = list(dicho_sort(end_input_list))

    sort_list = []
    j = 0
    i = 0
    while i < len(sort_start_input_list) and j < len(sort_end_input_list):
        if sort_start_input_list[i] < sort_end_input_list[j]:
            sort_list.append(sort_start_input_list[i])
            i += 1
        else:
            sort_list.append(sort_end_input_list[j])
            j += 1

    if i < len(sort_start_input_list):
        while i < len(sort_start_input_list):
            sort_list.append(sort_start_input_list[i])
            i += 1
    else:
        while j < len(sort_end_input_list):
            sort_list.append(sort_end_input_list[j])
            j += 1

    return sort_list


def recherche_dicho(element, liste):
    if len(liste) == 1:
        return 0
    list_size = len(liste) // 2
    if liste[list_size] == element:
        return list_size
    elif liste[list_size] > element:
        return recherche_dicho(element, liste[:list_size])
    else:
        return list_size + recherche_dicho(element, liste[list_size:])


def dicho_find(input_list, find_me):
    if len(input_list) < 1:
        return False

    if input_list[0] == find_me:
        return True

    sorted_input_list = dicho_sort(input_list)

    return recherche_dicho(find_me, sorted_input_list)

def get_diamond(diamond_size):
    if diamond_size % 2 == 0:
        print("None")
        return

    for i in range(1, diamond_size + 1, 1):
        if i % 2 == 1:
            print((diamond_size - i // 2) * " " + i * "*")
    for j in range(diamond_size - 1, 0, -1):
        if j % 2 == 1:
            print((diamond_size - j // 2) * " " + j * "*")


UNSORTED_LIST = [1, 5, 6, 2, 3, 8, 9, 7, 0, 4]
SORTED_LIST = dicho_sort(UNSORTED_LIST)

print("Unsorted list: " + str(UNSORTED_LIST))
print("Sorted list: " + str(SORTED_LIST))
print("Find (8) ? " + str(dicho_find(SORTED_LIST, 8)))

get_diamond(5)
