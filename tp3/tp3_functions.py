"""
    Représente le module des graphs
"""
import networkx as nwx
import matplotlib.pyplot as plt

def get_graph(edges_list):
    """
        Crée un graph orienté depuis une liste de tuples
    """
    graph = nwx.DiGraph()
    graph.add_edges_from(edges_list)
    return graph

def get_lamport_time(directed_graph):

    directed_graph.nodes['p_0_1']['e_0_1'] = 16
    directed_graph['p_0_1']['p_0_2']['m12'] = 32
    return directed_graph

def get_cuts(directed_graph):
    graph_pedia = {}
    nodes = directed_graph.nodes
    for node in nodes:
        graph_pedia[node] = {}
        graph_pedia[node]['nodes'] = oriented_graph.neighbors(node)
        graph_pedia[node]['mes'] = nodes[node]

    return graph_pedia


graph_tuple = [('p_0_1', 'p_0_2'),('p_0_2', 'p_0_3')]
oriented_graph = get_graph(graph_tuple)

o = get_cuts(oriented_graph)
print(o)

#o = get_lamport_time(oriented_graph)
#print(oriented_graph.nodes())
#for toto in oriented_graph.neighbors('p_0_2'):
#    print(toto)
#for toto in oriented_graph.predecessors('p_0_2'):
#    print(toto)


#nwx.draw(o)

plt.savefig("mygraph.png")
